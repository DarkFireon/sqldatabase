/
BEGIN
DBMS_SCHEDULER.create_job (
    job_name        => 'GENERUJ_TRANSAKCJE_CO_GODZINE',
    job_type        => 'STORED_PROCEDURE',
    job_action      => 'PR_i7y3_04_GENERUJ_LOSOWA_TRANSAKCJE',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'freq=hourly; byminute=0',
    end_date        => SYSDATE+2,
    auto_drop       => false,
    enabled         => TRUE,
    comments        => 'Generuje nowa transakcje co godzine przez 2 dni poczawszy od czasu stworzenia.');


END;
/