CREATE TABLE i7y3_04_faktura_detale (
    kod_faktury   NUMBER(5) NOT NULL,
    pozycja       NUMBER(2) NOT NULL,
    kod_piwa      NUMBER(5) NOT NULL,
    cena          NUMBER(10, 2),
    ilosc         NUMBER(8)
)
LOGGING;

ALTER TABLE i7y3_04_faktura_detale ADD CONSTRAINT i7y3_04_faktura_detale_pk PRIMARY KEY ( kod_faktury,
                                                                                          pozycja );

CREATE TABLE i7y3_04_faktura_naglowek (
    kod_faktury       NUMBER(5) NOT NULL,
    kod_klienta       NUMBER(5) NOT NULL,
    wartosc_faktury   NUMBER(15, 2),
    data_faktury      DATE
)
LOGGING;

ALTER TABLE i7y3_04_faktura_naglowek ADD CONSTRAINT i7y3_04_faktura_naglowek_pk PRIMARY KEY ( kod_faktury );

CREATE TABLE i7y3_04_klient (
    kod_klienta       NUMBER(5) NOT NULL,
    nazwisko          VARCHAR2(30),
    imie              VARCHAR2(30),
    adres             VARCHAR2(50),
    kod_wojewodztwa   NUMBER(5) NOT NULL,
    miasto            VARCHAR2(30)
)
LOGGING;

ALTER TABLE i7y3_04_klient ADD CONSTRAINT i7y3_04_klient_pk PRIMARY KEY ( kod_klienta );

CREATE TABLE i7y3_04_kraj_producenta (
    kod_kraju   NUMBER(5) NOT NULL,
    kraj        VARCHAR2(40)
)
LOGGING;

ALTER TABLE i7y3_04_kraj_producenta ADD CONSTRAINT i7y3_04_kraj_producenta_pk PRIMARY KEY ( kod_kraju );

CREATE TABLE i7y3_04_piwo (
    kod_piwa    NUMBER(5) NOT NULL,
    nazwa       VARCHAR2(40),
    cena        NUMBER(10, 2),
    kod_kraju   NUMBER(5) NOT NULL
)
LOGGING;

ALTER TABLE i7y3_04_piwo ADD CONSTRAINT i7y3_04_piwo_pk PRIMARY KEY ( kod_piwa );

CREATE TABLE i7y3_04_wojewodztwo (
    kod_wojewodztwa     NUMBER(5) NOT NULL,
    nazwa_wojewodztwa   VARCHAR2(40)
)
LOGGING;

ALTER TABLE i7y3_04_wojewodztwo ADD CONSTRAINT i7y3_04_wojewodztwo_pk PRIMARY KEY ( kod_wojewodztwa );

ALTER TABLE i7y3_04_faktura_detale
    ADD CONSTRAINT i7y3_04_faktura_naglowek_fk FOREIGN KEY ( kod_faktury )
        REFERENCES i7y3_04_faktura_naglowek ( kod_faktury )
            ON DELETE CASCADE
    NOT DEFERRABLE;

ALTER TABLE i7y3_04_faktura_naglowek
    ADD CONSTRAINT i7y3_04_klient_fk FOREIGN KEY ( kod_klienta )
        REFERENCES i7y3_04_klient ( kod_klienta )
            ON DELETE CASCADE
    NOT DEFERRABLE;

ALTER TABLE i7y3_04_piwo
    ADD CONSTRAINT i7y3_04_kraj_producenta_fk FOREIGN KEY ( kod_kraju )
        REFERENCES i7y3_04_kraj_producenta ( kod_kraju )
            ON DELETE CASCADE
    NOT DEFERRABLE;

ALTER TABLE i7y3_04_faktura_detale
    ADD CONSTRAINT i7y3_04_piwo_fk FOREIGN KEY ( kod_piwa )
        REFERENCES i7y3_04_piwo ( kod_piwa )
            ON DELETE CASCADE
    NOT DEFERRABLE;

ALTER TABLE i7y3_04_klient
    ADD CONSTRAINT i7y3_04_wojewodztwo_fk FOREIGN KEY ( kod_wojewodztwa )
        REFERENCES i7y3_04_wojewodztwo ( kod_wojewodztwa )
            ON DELETE CASCADE
    NOT DEFERRABLE;