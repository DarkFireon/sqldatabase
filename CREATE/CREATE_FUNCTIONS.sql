create or replace FUNCTION FN_i7y3_04_KLIENT RETURN NUMBER AS 
v_rand number(10);
BEGIN
 select * into v_rand
 from (
	select kod_klienta from  i7y3_04_klient
		order by dbms_random.value)
where rownum = 1;
return v_rand;
END FN_i7y3_04_KLIENT;
/
create or replace FUNCTION FN_i7y3_04_piwo RETURN NUMBER AS 
v_rand number(10);
BEGIN
 select * into v_rand
 from (
	select kod_piwa from  i7y3_04_piwo
		order by dbms_random.value)
where rownum = 1;
return v_rand;
END FN_i7y3_04_piwo;
/
  CREATE OR REPLACE FUNCTION FN_i7y3_04_piwo_CENA (v_kod_piwa i7y3_04_piwo.kod_piwa%type ) RETURN i7y3_04_piwo.cena%type 
  AS 
v_cena i7y3_04_piwo.cena%type;
BEGIN
  select cena into v_cena from i7y3_04_piwo 
where kod_piwa = v_kod_piwa;
RETURN v_cena;
END FN_i7y3_04_piwo_CENA;
/

  CREATE OR REPLACE FUNCTION FN_i7y3_04_piwo_ILOSC RETURN NUMBER AS 
v_ilosc NUMBER(3);
BEGIN
    select round (dbms_random.value(1, 20)) INTO v_ilosc  from dual;
  RETURN v_ilosc;
END FN_i7y3_04_piwo_ILOSC;
/

