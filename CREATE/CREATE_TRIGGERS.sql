CREATE OR REPLACE TRIGGER tr_i7y3_04_faktura_detale 
    BEFORE INSERT OR DELETE OR UPDATE ON i7y3_04_faktura_detale 
    FOR EACH ROW 
DECLARE PRICE_UNIT NUMERIC(8,2);

 BEGIN
	  if INSERTING then
	  
        select cena into PRICE_UNIT
        from i7y3_04_piwo  where kod_piwa=:NEW.kod_piwa;
		:NEW.cena := PRICE_UNIT;
        
	    update i7y3_04_faktura_naglowek 
	    	set wartosc_faktury = wartosc_faktury + (:NEW.cena * :NEW.ilosc)
	    where kod_faktury = :NEW.kod_faktury;

	  end if;

	  if UPDATING then
		 if (:NEW.kod_piwa = :OLD.kod_piwa) then
	
			update i7y3_04_faktura_naglowek 
		    	set wartosc_faktury = wartosc_faktury + (:OLD.cena*:NEW.ilosc) - (:OLD.cena*:OLD.ilosc)
		    where kod_faktury = :NEW.kod_faktury ;
	
		 else
	        select cena into PRICE_UNIT
	        from i7y3_04_piwo where kod_piwa=:NEW.kod_piwa;
			:NEW.cena := PRICE_UNIT;
	
		    update i7y3_04_faktura_naglowek 
		    	set wartosc_faktury = wartosc_faktury + (:NEW.cena*:NEW.ilosc) - (:OLD.cena*:OLD.ilosc)
		    where kod_faktury = :NEW.kod_faktury;
		end if;
	  end if;

	  if DELETING then
	  
	    update i7y3_04_faktura_naglowek 
	    	set wartosc_faktury = wartosc_faktury -  (:OLD.cena*:OLD.ilosc)
	    where kod_faktury = :OLD.kod_faktury;
	    
	  end if;
END; 
/

CREATE OR REPLACE TRIGGER tr_ins_i7y3_04_klient 
    BEFORE INSERT ON i7y3_04_klient 
    FOR EACH ROW 
BEGIN
	:NEW.kod_klienta := seq_i7y3_04_ins_klient.nextval;
END; 
/

CREATE OR REPLACE TRIGGER tr_ins_i7y3_04_kraj
    BEFORE INSERT ON i7y3_04_kraj_producenta 
    FOR EACH ROW 
BEGIN
	:NEW.kod_kraju := seq_i7y3_04_ins_kraj.nextval;
END; 
/

CREATE OR REPLACE TRIGGER tr_ins_i7y3_04_piwo 
    BEFORE INSERT ON i7y3_04_piwo 
    FOR EACH ROW 
BEGIN
	:NEW.kod_piwa := seq_i7y3_04_ins_piwo.nextval;
END; 
/

CREATE OR REPLACE TRIGGER tr_ins_i7y3_04_wojewodztwo
    BEFORE INSERT ON i7y3_04_wojewodztwo 
    FOR EACH ROW 
BEGIN
	:NEW.kod_wojewodztwa := seq_i7y3_04_ins_wojewodztwo.nextval;
END; 
/

CREATE OR REPLACE TRIGGER tr_ins_i7y3_04_faktura 
    BEFORE INSERT ON i7y3_04_faktura_naglowek 
    FOR EACH ROW 
BEGIN
	:NEW.kod_faktury := seq_i7y3_04_ins_faktura.nextval;
	:NEW.data_faktury := sysdate;
END; 
/