
prompt Tworze sekwencje.
@CREATE_SEQUENCES
prompt Sekwencje stworzone.

prompt Tworze tabele.
@CREATE_TABLES
prompt Tabele stworzone.

prompt Tworze triggery.
@CREATE_TRIGGERS
prompt Triggery stworzone.

prompt Tworze funkcje.
@CREATE_FUNCTIONS
prompt Funkcje stworzone.

prompt Tworze procedury.
@CREATE_PROCEDURES
prompt Procedury stworzone.

prompt Tworze perspektywy.
@CREATE_VIEWS
prompt Perspektywy stworzone.

prompt Tworze joby.
@CREATE_JOBS
prompt Joby stworzone.