/
CREATE OR REPLACE PROCEDURE PR_i7y3_04_GENERUJ_POJ_POZYCJE (v_kod_faktury in i7y3_04_faktura_naglowek.kod_faktury%type,
					v_pozycja in i7y3_04_faktura_detale.pozycja%type)
 AS 
v_kod_piwa number;
v_cena i7y3_04_piwo.cena%type;
v_ilosc number;
BEGIN
    DBMS_OUTPUT.ENABLE;
    v_kod_piwa := FN_i7y3_04_piwo;
    v_cena := FN_i7y3_04_piwo_CENA(v_kod_piwa);
    v_ilosc := FN_i7y3_04_piwo_ILOSC;
    
    insert into i7y3_04_faktura_detale(kod_faktury, pozycja, cena, ilosc, kod_piwa) VALUES (
    v_kod_faktury,
    v_pozycja,
    v_cena,
    v_ilosc,
    v_kod_piwa
    );
END PR_i7y3_04_GENERUJ_POJ_POZYCJE;

/

CREATE OR REPLACE PROCEDURE PR_i7y3_04_GENERUJ_POZYCJE (v_kod_faktury in i7y3_04_faktura_naglowek.kod_faktury%type) AS 
v_ilosc_pozycji number;
BEGIN
    select round( dbms_random.value ( 1, 7 )) INTO v_ilosc_pozycji from dual;
    for v_counter in 1..v_ilosc_pozycji
    loop
        PR_i7y3_04_GENERUJ_POJ_POZYCJE(v_kod_faktury, v_counter);    
    end loop;

END PR_i7y3_04_GENERUJ_POZYCJE;
/ 




 




create or replace PROCEDURE PR_i7y3_04_GENERUJ_faktura_naglowek (v_kod_faktury out i7y3_04_faktura_naglowek.kod_faktury%type) AS 
BEGIN
  insert into i7y3_04_faktura_naglowek(kod_klienta,wartosc_faktury) values(
  FN_i7y3_04_KLIENT,
  0
  );
   v_kod_faktury := SEQ_i7y3_04_INS_faktura.CURRVAL;
END PR_i7y3_04_GENERUJ_faktura_naglowek;
/



  CREATE OR REPLACE PROCEDURE PR_i7y3_04_GENERUJ_LOSOWA_TRANSAKCJE
  as
	v_kod_faktury i7y3_04_faktura_naglowek.kod_faktury%type;
BEGIN
    PR_i7y3_04_GENERUJ_faktura_naglowek(v_kod_faktury);
    PR_i7y3_04_GENERUJ_POZYCJE(v_kod_faktury);
END PR_i7y3_04_GENERUJ_LOSOWA_TRANSAKCJE;
/