create or replace view piwa_zyski as
SELECT nazwa , kraj  ,SUM(ilosc) as ilosc,i7y3_04_piwo.cena*SUM(ilosc) as zysk 
from i7y3_04_kraj_producenta,i7y3_04_faktura_detale, i7y3_04_piwo 
WHERE 
i7y3_04_faktura_detale.kod_piwa = i7y3_04_piwo.kod_piwa
and i7y3_04_kraj_producenta.kod_kraju = i7y3_04_piwo.kod_kraju
group by nazwa,kraj,i7y3_04_piwo.cena
order by  zysk desc;
/*
create view faktury_zyski as
SELECT DISTINCT data_faktury  as "data" ,count(data_faktury) as ilosc ,
 ROUND(sum(wartosc_faktury),2)  as zysk 
 FROM i7y3_04_faktura_naglowek
 group by data_faktury
order by zysk desc;

create view faktury_ostatni_miesiac as
SELECT  *
 FROM i7y3_04_faktura_naglowek
 where data_faktury between sysdate-30 and sysdate
order by data_faktury;
*/
create or replace view najlepsi_klienci as
SELECT INITCAP(imie) as imie ,INITCAP(nazwisko) as nazwisko,count(wartosc_faktury) as ilosc_zamowien , ROUND(SUM(wartosc_faktury),2) as wydatki from 
i7y3_04_faktura_naglowek, i7y3_04_klient
WHERE i7y3_04_faktura_naglowek.kod_klienta = i7y3_04_klient.kod_klienta
GROUP BY INITCAP(imie),INITCAP(nazwisko)
order by wydatki desc;

create or replace view ewidencja_miesiecy as
SELECT  DISTINCT extract(year from data_faktury) as  rok, to_char(sysdate, 'Month') as miesiac, 
count(data_faktury) as ilosc ,
(select ROUND(sum(wartosc_faktury),2) 
from i7y3_04_faktura_naglowek ) as zysk 
FROM i7y3_04_faktura_naglowek
group by extract(year from data_faktury) 
order by rok,zysk desc;
