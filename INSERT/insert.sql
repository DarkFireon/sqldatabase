prompt Uzupelniam wojewodztwa.
@wojewodztwa
prompt Skonczylem uzupelniac wojewodztwa.

prompt Uzupelniam klientow.
@klienci
prompt Skonczylem uzupelniac klientow.

prompt Uzupelniam kraje producentow.
@producenci
prompt Skonczylem uzupelniac kraje producentow.

prompt Uzupelniam piwa.
@piwa
prompt Skonczylem uzupelniac piwa.

prompt Uzupelniam generatorem dane;
/
BEGIN
DBMS_SCHEDULER.create_job (
    job_name        => 'GENERUJ_TRANSAKCJE_CO_2_SEKUNDY',
    job_type        => 'STORED_PROCEDURE',
    job_action      => 'PR_i7y3_04_GENERUJ_LOSOWA_TRANSAKCJE',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'FREQ=SECONDLY;INTERVAL=2',
    end_date        =>SYSTIMESTAMP+1/(24*60),
    enabled         => TRUE,
    auto_drop       => false,
    comments        => 'Generuje nowa transakcje co 2 sekundY przez 1 min poczawszy od czasu stworzenia.');
END;
/
prompt Skonczylem uzupelniac dane;