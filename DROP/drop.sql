
prompt Usuwam sekwencje.
@DROP_SEQUENCES
prompt Sekwencje usuniete.

prompt Usuwam tabele.
@DROP_TABLES
prompt Tabele usuniete.

prompt Usuwam funkcje.
@DROP_FUNCTIONS
prompt Funkcje usuniete.

prompt Usuwam procedury.
@DROP_PROCEDURES
prompt Procedury usuniete.

prompt Usuwam perspektywy.
@DROP_VIEWS
prompt Perspektywy usuniete.

prompt Usuwam joby.
@DROP_JOBS;
prompt Joby usuniete.